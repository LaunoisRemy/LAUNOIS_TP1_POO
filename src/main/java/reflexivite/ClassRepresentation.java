package reflexivite;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Parameter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ClassRepresentation {

    private String name;
    private String visibility;
    private List<MethodRepresentation> methods;
    private Class<?> superClass;
    private Class<?>[] interfaces;
    private Package packageClass;

    public ClassRepresentation(Class<?> className) {
        name = className.getSimpleName();
        superClass = className.getSuperclass();
        interfaces = className.getInterfaces();
        packageClass = className.getPackage();
        methods = new ArrayList<>();
    }

    public void addMethod(MethodRepresentation method) {
        methods.add(method);
    }

    public void addAllMethods(List<MethodRepresentation> method) {
        this.methods.addAll(method);
    }

    public void addMethodsByClassName(Class<?> className) {

        Method[] declaredMethods = className.getDeclaredMethods();

        Arrays.stream(declaredMethods).forEach(method -> {

            String name = method.getName();
            Parameter[] parameters = method.getParameters();
            String returnType = method.getReturnType().getSimpleName();
            String visibility = Modifier.toString(method.getModifiers());

            addMethod(new MethodRepresentation(name, parameters, visibility, returnType));
        });


    }

    public String convertToInterface() {

        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append("public interface ").append("I").append(name).append(" {\n");

        methods.stream().filter(method -> method.getVisibility().equals("public")).forEach(method -> {
            stringBuilder.append("\t").append(method.toInterfaceMethod()).append("\n");
        });

        stringBuilder.append("}");

        return stringBuilder.toString();
    }

    @Override
    public String toString() {

        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append("Name : ").append(name);
        stringBuilder.append("\n");
        stringBuilder.append("Methods : \n");

        methods.forEach(method -> {
            stringBuilder.append(method).append("\n");
        });

        return stringBuilder.toString();
    }
}
