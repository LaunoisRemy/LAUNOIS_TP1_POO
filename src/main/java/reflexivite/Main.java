package reflexivite;

import java.io.IOException;

public class Main {

    public static void main(String[] args) throws IOException {

        ClassToInterface c1 = new ClassToInterface(Appartement.class);
        ClassToInterface c2 = new ClassToInterface(AppartementResidence.class);

        System.out.println(c1);
        System.out.println(c2);

        c1.createFile();
        c2.createFile();
    }
}
