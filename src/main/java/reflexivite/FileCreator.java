package reflexivite;

import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class FileCreator {

    private final String filename;
    private final ClassRepresentation classe;

    public FileCreator(String filename, ClassRepresentation classe) {
        this.filename = filename;
        this.classe = classe;
    }

    public void toInterface() throws IOException {

        Path path = Paths.get(filename);
        OutputStream outputStream = Files.newOutputStream(path);
        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(outputStream, StandardCharsets.UTF_8);
        outputStreamWriter.write(classe.convertToInterface());
        outputStreamWriter.close();
    }
}
