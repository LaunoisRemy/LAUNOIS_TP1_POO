package reflexivite;

import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Parameter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.StringJoiner;
import java.util.stream.Collectors;

public class ClassToInterface {

    private final Class<?> classe;

    public ClassToInterface(Class<?> classe) {
        this.classe = classe;
    }

    public String header() {

        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append("public interface I").append(classe.getSimpleName()).append(" extends I").append(classe.getSuperclass().getSimpleName());

        Arrays.stream(classe.getInterfaces()).forEach(classe -> stringBuilder.append(", ").append(classe.getSimpleName()));

        stringBuilder.append(" {");

        return stringBuilder.toString();
    }

    public String methods() {

        return Arrays.stream(classe.getDeclaredMethods())
                .filter(method -> Modifier.isPublic(method.getModifiers()) && !Modifier.isStatic(method.getModifiers()) && !isOverriden(method))
                .map(ClassToInterface::writeMethod)
                .collect(Collectors.joining());
    }

    public static String writeMethod(Method method) {

        StringJoiner stringJoiner = new StringJoiner(", ");

        for (Parameter parameter : method.getParameters()) {
            stringJoiner.add(parameter.getType().getSimpleName() + " " + parameter.getName());
        }

        return "\t" + method.getReturnType().getSimpleName() + " " + method.getName() + "(" + stringJoiner.toString() + ");\n";
    }


    public boolean isOverriden(Method method) {

        try {
            Method superMethod = classe.getSuperclass().getDeclaredMethod(method.getName());

            return Arrays.equals(superMethod.getParameters(), method.getParameters());
        } catch (NoSuchMethodException e) {
            return false;
        }
    }

    public void createFile() throws IOException {

        Path path = Paths.get("src/main/java/reflexivite/"+"I" + classe.getSimpleName() + ".java");

        OutputStream outputStream = Files.newOutputStream(path);
        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(outputStream, StandardCharsets.UTF_8);
        outputStreamWriter.write(this.toString());
        outputStreamWriter.close();
    }

    @Override
    public String toString() {
        return header() + "\n" + methods() + "}";
    }
}