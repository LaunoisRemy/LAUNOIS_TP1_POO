package reflexivite;

import java.lang.reflect.Parameter;

public class MethodRepresentation {

    private final String name;
    private final Parameter[] parameters;
    private final String visibility;
    private final String returnType;

    public MethodRepresentation(String name, Parameter[] parameters, String visibility, String returnType) {
        this.name = name;
        this.parameters = parameters;
        this.visibility = visibility;
        this.returnType = returnType;
    }

    public String getName() {
        return name;
    }

    public Parameter[] getParameters() {
        return parameters;
    }

    public String getVisibility() {
        return visibility;
    }

    public String getReturnType() {
        return returnType;
    }

    public String toInterfaceMethod() {

        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append(returnType).append(" ");
        stringBuilder.append(name);
        stringBuilder.append("(");

        for (int i = 0; i < parameters.length; i++) {
            stringBuilder.append(parameters[i].getType().getSimpleName()).append(" ").append(parameters[i].getName());

            if (i != parameters.length - 1) {
                stringBuilder.append(", ");
            }
        }

        stringBuilder.append(");");

        return stringBuilder.toString();
    }

    @Override
    public String toString() {

        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append(visibility).append(" ");
        stringBuilder.append(returnType).append(" ");
        stringBuilder.append(name);
        stringBuilder.append("(");

        for (int i = 0; i < parameters.length; i++) {
            stringBuilder.append(parameters[i].getType().getSimpleName()).append(" ").append(parameters[i].getName());

            if (i != parameters.length - 1) {
                stringBuilder.append(", ");
            }
        }

        stringBuilder.append(")");

        return stringBuilder.toString();
    }
}
