package solid.single.responsibility;

import java.io.FileWriter;
import java.io.IOException;

/**
 * @author Launois Remy
 * Project: LAUNOIS_TP1_POO
 * Package: single.responsibility.good.implementation
 */
public class WriteFile {
    public void writeFile(String fileName,String content){
        try {

            FileWriter fileWriter = new FileWriter("src/main/resources/"+fileName);
            fileWriter.write(content);
            fileWriter.close();
        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }

    }
}
