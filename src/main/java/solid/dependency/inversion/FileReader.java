package solid.dependency.inversion;

/**
 * @author Launois Remy
 * Project: LAUNOIS_TP1_POO
 * Package: open.close
 */
public interface FileReader {
    String readFile(String fileName);
    void printFile(String fileName);
    void printFirstLine(String fileName);

}
