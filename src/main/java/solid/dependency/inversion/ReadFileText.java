package solid.dependency.inversion;

import java.io.File;
import java.io.FileNotFoundException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Scanner;

/**
 * @author Launois Remy
 * Project: LAUNOIS_TP1_POO
 * Package: single.responsibility
 */
public class ReadFileText implements FileReader {

    /**
     * Method to save content of a txt file in a StringBuilder
     * @param fileName Name of file, without the type of file
     * @return StringBuilder which contains the content
     */
    public String readFile(String fileName){
        try {
            StringBuilder contentFile = new StringBuilder();
            URL resource = getClass().getClassLoader().getResource(fileName+".txt");
            File file ;
            if (resource == null) {
                throw new IllegalArgumentException("file not found!");
            } else {
                file = new File(resource.toURI());
            }
            Scanner myReader = new Scanner(file);

            while (myReader.hasNextLine()) {
                contentFile.append(myReader.nextLine()).append("\n");
            }

            myReader.close();
            return contentFile.toString();
        } catch (FileNotFoundException | URISyntaxException e) {
            return e.getMessage();
        }
    }

    public void printFile(String fileName){
        System.out.println(this.readFile(fileName));

    }

    public void printFirstLine(String fileName){
        System.out.println(this.readFile((fileName)).toString().split("\\n")[0]);


    }
}
