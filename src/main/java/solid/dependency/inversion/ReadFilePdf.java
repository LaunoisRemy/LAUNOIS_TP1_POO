package solid.dependency.inversion;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;

/**
 * @author Launois Remy
 * Project: LAUNOIS_TP1_POO
 * Package: open.close
 */
public class ReadFilePdf implements FileReader {
    @Override
    public String readFile(String fileName) {

        try {
            URL resource = getClass().getClassLoader().getResource(fileName+".pdf");
            File file ;
            if (resource == null) {
                throw new IllegalArgumentException("file not found!");
            } else {
                file = new File(resource.toURI());
            }
            PDDocument document = PDDocument.load(file);
            PDFTextStripper pdfStripper = new PDFTextStripper();
            String text = pdfStripper.getText(document);
            document.close();
            return text;
        } catch (IOException | URISyntaxException e) {
            return e.getMessage();
        }
    }

    @Override
    public void printFile(String fileName) {

    }

    @Override
    public void printFirstLine(String fileName) {

    }
}
