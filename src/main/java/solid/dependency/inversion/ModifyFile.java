package solid.dependency.inversion;

/**
 * @author Launois Remy
 * Project: LAUNOIS_TP1_POO
 * Package: dependency.inversion
 */
public class ModifyFile {
    private FileReader fileReader;
    private WriterFile writerFile;

    public ModifyFile() {
        this.fileReader = new ReadFileText();
        this.writerFile = new WriteFileTxt();
    }

    public void copyFile(){
        String content = fileReader.readFile("text");
        writerFile.writeFile("texte3.txt",content);
    }
}
