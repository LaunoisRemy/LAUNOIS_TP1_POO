package solid.dependency.inversion;

/**
 * @author Launois Remy
 * Project: LAUNOIS_TP1_POO
 * Package: dependency.inversion
 */
public interface WriterFile {
    void writeFile(String fileName,String content);
}
