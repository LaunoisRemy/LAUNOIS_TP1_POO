package solid.interfacesegregation;

/**
 * @author Launois Remy
 * Project: LAUNOIS_TP1_POO
 * Package: liskov.substitution
 */
public class Vulture extends FlyingsBirds implements Carnivore{
    public Vulture(int sizeOfWings, String typeOfBeak, String typeOfFeather, int flyingTime) {
        super(sizeOfWings, typeOfBeak, typeOfFeather, flyingTime);
    }

    @Override
    public void hunt() {
        System.out.println("search carcass");
    }
}
