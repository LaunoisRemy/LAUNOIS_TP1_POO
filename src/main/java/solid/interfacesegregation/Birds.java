package solid.interfacesegregation;

/**
 * @author Launois Remy
 * Project: LAUNOIS_TP1_POO
 * Package: liskov.substitution
 */
public abstract class Birds {
    private int sizeOfWings;
    private String typeOfBeak;
    private String typeOfFeather;

    public Birds(int sizeOfWings, String typeOfBeak, String typeOfFeather) {
        this.sizeOfWings = sizeOfWings;
        this.typeOfBeak = typeOfBeak;
        this.typeOfFeather = typeOfFeather;
    }

    public int getSizeOfWings() {
        return sizeOfWings;
    }

    public void setSizeOfWings(int sizeOfWings) {
        this.sizeOfWings = sizeOfWings;
    }

    public String getTypeOfBeak() {
        return typeOfBeak;
    }

    public void setTypeOfBeak(String typeOfBeak) {
        this.typeOfBeak = typeOfBeak;
    }

    public String getTypeOfFeather() {
        return typeOfFeather;
    }

    public void setTypeOfFeather(String typeOfFeather) {
        this.typeOfFeather = typeOfFeather;
    }
}
