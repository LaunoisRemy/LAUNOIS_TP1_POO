package solid.interfacesegregation;

import solid.liskov.substitution.Birds;

/**
 * @author Launois Remy
 * Project: LAUNOIS_TP1_POO
 * Package: liskov.substitution
 */
public class FlyingsBirds extends Birds {
    private int flyingTime;

    public FlyingsBirds(int sizeOfWings, String typeOfBeak, String typeOfFeather, int flyingTime) {
        super(sizeOfWings, typeOfBeak, typeOfFeather);
        this.flyingTime = flyingTime;
    }

    public void fly(){
        System.out.println("I can fly :"+flyingTime+" minutes !!!");

    }
}
