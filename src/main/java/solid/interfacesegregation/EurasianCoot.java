package solid.interfacesegregation;

/**
 * @author Launois Remy
 * Project: LAUNOIS_TP1_POO
 * Package: interfacesegregation
 */
public class EurasianCoot extends FlyingsBirds implements Herbivores, BirdsMigrating{
    public EurasianCoot(int sizeOfWings, String typeOfBeak, String typeOfFeather, int flyingTime) {
        super(sizeOfWings, typeOfBeak, typeOfFeather, flyingTime);
    }

    @Override
    public void searchFood() {
        System.out.println("Dive in water to take food");
    }

    @Override
    public void migrate() {
        System.out.println("Migrate to south of Asia");
    }
}
