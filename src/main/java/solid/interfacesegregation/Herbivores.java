package solid.interfacesegregation;

/**
 * @author Launois Remy
 * Project: LAUNOIS_TP1_POO
 * Package: interfacesegregation
 */
public interface Herbivores {
    void searchFood();
}
