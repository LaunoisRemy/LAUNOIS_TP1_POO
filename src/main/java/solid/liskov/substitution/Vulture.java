package solid.liskov.substitution;

import solid.interfacesegregation.FlyingsBirds;

/**
 * @author Launois Remy
 * Project: LAUNOIS_TP1_POO
 * Package: liskov.substitution
 */
public class Vulture extends FlyingsBirds {
    public Vulture(int sizeOfWings, String typeOfBeak, String typeOfFeather, int flyingTime) {
        super(sizeOfWings, typeOfBeak, typeOfFeather, flyingTime);
    }
}
