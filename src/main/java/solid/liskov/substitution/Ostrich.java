package solid.liskov.substitution;

/**
 * @author Launois Remy
 * Project: LAUNOIS_TP1_POO
 * Package: liskov.substitution
 */
public class Ostrich extends Birds{
    public Ostrich(int sizeOfWings, String typeOfBeak, String typeOfFeather) {
        super(sizeOfWings, typeOfBeak, typeOfFeather);
    }
}
