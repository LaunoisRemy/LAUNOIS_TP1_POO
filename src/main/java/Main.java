import solid.open.close.ReadFilePdf;
import solid.single.responsibility.ReadFile;
import solid.single.responsibility.WriteFile;
/**
 * @author Launois Remy
 * Project LAUNOIS_TP1_POO
 */
public class Main {
    public static void main(String[] args) {
        System.out.println("Appel de la classe qui ne respecte pas single responsibility");
        ReadFile readFile = new ReadFile();
        String content = readFile.readFile("text").toString();
        System.out.println(content);
        content+="\nDeuxieme fichier";
        WriteFile writeFile = new WriteFile();
        writeFile.writeFile("text2.txt",content);
        System.out.println(readFile.readFile("text2"));



        ReadFilePdf readFilePdf = new ReadFilePdf();
        System.out.println(readFilePdf.readFile("tp1Aut2021"));


    }
}
